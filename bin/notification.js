var redis = require('redis');
require('dotenv').config();
var socketioJwt = require('socketio-jwt');
var jwt = require('jsonwebtoken');
require('dotenv').config();

const type_event = {
    TO_USER: 1,
    TO_ROOM: 2,
    TO_ALL: 3
}

const config = {
    AUTH0_CLIENT_SECRET: process.env.AUTH0_CLIENT_SECRET || 'loyalty',
    PORT_SOCKET: process.env.PORT_SOCKET || 5000,
    ROOM_ALL: "all"
};

var express = require('express'),
    app = express(),
    server = require('http').createServer(app),
    io = require('socket.io').listen(server);


var client = redis.createClient();

io.sockets.on('connection', socketioJwt.authorize({
    secret: config.AUTH0_CLIENT_SECRET,
    timeout: 1500
})).on('authenticated', function (socket) {
    socket.on('push notification', function (data) {
        client.get("notification:" + socket.decoded_token.user_id, (err, res) => {
            res = JSON.parse(res)
            if (!res.socket_id) {
                if(socket.decoded_token.room) socket.join(data.room);
                client.set("notification:" + socket.decoded_token.user_id, JSON.stringify({token: res.token, socket_id: socket.id}));
            }
            if (data.type_event == type_event.TO_ROOM) {
                console.log('send to: ' + data.room)
                io.in(data.room).clients((err, clis) => {
                    io.sockets.in(data.room).emit('message', data.message)
                });
            }
            if (data.type_event == type_event.TO_USER) {
                console.log('send to: ' + data.sendto)
                client.get(data.sendto, (err, res) => {
                    if(res){
                        res = JSON.parse(res);
                        if (res.socket_id) {
                            io.to(res.socket_id).emit('message', data.message);
                        }
                    }
                });
            }
            if (data.type_event == type_event.TO_ALL) {
                console.log('send all');
                socket.broadcast.emit('message', data.message)
            }
        });
    });
});

server.listen(config.PORT_SOCKET, function () {
    console.log('open in port : ' + config.PORT_SOCKET)
});
