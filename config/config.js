require('dotenv').config(); // this is important!
module.exports = {
    "development": {
        "username": process.env.PGSQL_USER,
        "password": process.env.PGSQL_PASS,
        "database": process.env.PGSQL_DATA,
        "host": process.env.PGSQL_HOST,
        "dialect": "postgresql"
    },
    "test": {
        "username": process.env.PGSQL_USER,
        "password": process.env.PGSQL_PASS,
        "database": process.env.PGSQL_DATA,
        "host": process.env.PGSQL_HOST,
        "dialect": "postgresql"
    },
    "production": {
        "username": process.env.PGSQL_USE,
        "password": process.env.PGSQL_PASS,
        "database": process.env.PGSQL_DATA,
        "host": process.env.PGSQL_HOST,
        "dialect": "postgresql"
    }
};
