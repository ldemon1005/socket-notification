require('dotenv').config();
var jwt = require('jsonwebtoken');
var redis = require('redis');
var client = redis.createClient();
const config = {
    AUTH0_CLIENT_SECRET: process.env.AUTH0_CLIENT_SECRET
};
function login(req,res,next){
    var data = req.body;
    var user = {
        "user_id": data.user_id,
        "email": data.email,
    };
    var token = jwt.sign(user,  config.AUTH0_CLIENT_SECRET, {expiresIn : 60*60*24});
    client.set("notification:" + data.user_id,JSON.stringify({token: token}));
    res.send({
        status: 200,
        msg: 'SUCCESS',
        data: token
    })
}

var UserController = {
    login
};

module.exports = UserController;
