const Notification = (sequelize, DataTypes) => sequelize.define('Notification', {
    type_event: {
        type: DataTypes.INTEGER,
        allowNull: true
    },
    room_name: {
        type: DataTypes.STRING(30),
        allowNull: true
    },
    user_id: {
        type: DataTypes.INTEGER,
        allowNull: true
    },
    sendto: {
        type: DataTypes.INTEGER,
        allowNull: true
    },
    message: {
        type: DataTypes.STRING(30),
        allowNull: true
    },
    createdAt: {
        allowNull: true,
        type: DataTypes.DATE,
    },
    updatedAt: {
        allowNull: true,
        type: DataTypes.DATE,
    }
}, {
    schema: 'public',
    tableName: 'Notifications',
    timestamps: true
})

module.exports = Notification
