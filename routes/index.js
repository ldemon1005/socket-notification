var express = require('express');
require('dotenv').config();
var router = express.Router();
const io = require('socket.io-client');
const Notification = require('../models').Notification;

var User = require('./../controller/user_controller');
/* GET users listing. */

router.get('', function (req, res, next) {
    res.send('hello notification');
});

router.post('/push-notification', function (req, res, next) {
    var data = req.body;
    var token = req.headers.authorization;
    var socket = io.connect(process.env.SOCKET_URI);
    console.log(Notification)
    socket.on('connect', function () {
        socket
            .emit('authenticate', {token: token})
            .on('authenticated', function () {
                return Notification.create({
                        room_name: data.room_name,
                        type_event: data.type_event,
                        user_id: data.user_id,
                        message: data.message,
                        sendto: data.sendto
                    })
                    .then(notify => {
                        socket.emit('push notification',
                            {
                                room: data.room_name,
                                type_event: data.type_event,
                                message: data.message,
                                sendto: data.sendto
                            });
                        return res.status(201).send(notify)
                    })
                    .catch(error => {
                        return res.status(400).send(error)
                    })
            })
            .on('unauthorized', function (msg) {
                return res.status(401).send(JSON.stringify(msg.data.message))
            })
    });
});

router.post('/login', function (req, res, next) {
    User.login(req, res, next)
});

module.exports = router;
