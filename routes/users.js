var express = require('express');
var router = express.Router();
const io = require('socket.io-client');
/* GET users listing. */
router.get('/', function (req, res, next) {
    var socket = io.connect('http://127.0.0.1:5001');
    var room = "abc12345";
    socket.on('connect', function () {
        socket.emit('push notification', {room: room, id: 1996});
    });
    var response_str = "Thông báo!";
    socket.on('message', function (data) {
        console.log('message: ',data);
        response_str = data;
    });
    res.send(response_str);
});

module.exports = router;
